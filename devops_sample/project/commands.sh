wget -O res.zip https://gitlab.com/ps.abdelrahman.alsawalmeh/devops-practice_new/-/jobs/artifacts/practice_changes/download?job=build_artifact
unzip res.zip
cd project/MavenProject/target/
echo ${SPRING_DATASOURCE_PASSWORD}
echo ${SPRING_DATASOURCE_URL}
java -jar -Dspring.datasource.username=${SPRING_DATASOURCE_USERNAME} -Dspring.datasource.password=${SPRING_DATASOURCE_PASSWORD} -Dspring.datasource.url=jdbc:mysql://${SPRING_DATASOURCE_URL}/assignment?allowPublicKeyRetrieval=true assignment-*.jar