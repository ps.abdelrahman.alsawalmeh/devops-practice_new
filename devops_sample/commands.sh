cd target/
echo ${SPRING_DATASOURCE_PASSWORD}
echo ${SPRING_DATASOURCE_URL}
java -jar -Dspring.datasource.username=${SPRING_DATASOURCE_USERNAME} -Dspring.datasource.password=${SPRING_DATASOURCE_PASSWORD} -Dspring.datasource.url=jdbc:mysql://${SPRING_DATASOURCE_URL}/assignment?allowPublicKeyRetrieval=true assignment-*.jar